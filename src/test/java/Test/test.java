package Test;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class test  {

	public WebDriver driver;

	@Test
	public void login() throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Shared/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.linkedin.com/login");
		Thread.sleep(5000);
		driver.findElement(By.id("username")).sendKeys("abc@gmail.com");
		driver.findElement(By.id("password")).sendKeys("abc@123");
		driver.findElement(By.cssSelector("button.btn__primary--large.from__button--floating")).click();
		Thread.sleep(5000);
		driver.close();
	}
}
